package de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.filter;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.BaseFilter;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.BaseFilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class BaseFilterInstanceCreatorImpl extends FilterInstanceCreatorImpl {
    @Override
    public FilterInstance create(FilterInstance instance, Filter filter) {
        BaseFilterInstance result = new BaseFilterInstance();
        BaseFilter base = (BaseFilter) filter;

        this.initialize(result, filter);
        result.setAscending(base.isAscending());
        result.setTasklistOrder(base.getTasklistOrder());
        return result;
    }
}
