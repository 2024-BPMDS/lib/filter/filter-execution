package de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.bpmElement;

import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.BpmElement;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElement.FilterBpmElementInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElement.FilterBpmElementState;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FilterBpmElementInstanceCreator {
    public List<FilterBpmElementInstance> create(List<BpmElement> elements) {
        return elements.stream().map(this::create).toList();
    }

    private FilterBpmElementInstance create(BpmElement element) {
        FilterBpmElementInstance instance = new FilterBpmElementInstance();
        instance.setId(null);
        instance.setState(FilterBpmElementState.NEUTRAL);
        instance.setRecproElementId(element.getId());
        return instance;
    }
}
