package de.ubt.ai4.petter.recpro.lib.filter.filterexecution.service;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.FilterType;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IFilterExecutionService {
    FilterInstance save(FilterInstance instance);
    List<FilterInstance> getAll();
    FilterInstance create(String filterId, Long tasklistId, String assigneeId, FilterType filterType);

    FilterInstance createDefault(Long tasklistId, String assigneeId, FilterType filterType);
    FilterInstance getLatestByAssigneeId(String assigneeId, Long tasklistId, FilterType filterType);
    FilterInstance getLatestByFilterInstance(FilterInstance instance);
    FilterInstance getByTasklistIdAndFilterType(Long tasklistId, String assigneeId, FilterType filterType);
    FilterInstance create(FilterInstance instance);
    FilterInstance create(String filterId, String assigneeId, Tasklist tasklist, FilterType filterType);
}
