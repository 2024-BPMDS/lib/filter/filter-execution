package de.ubt.ai4.petter.recpro.lib.filter.filterexecution.service;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.FilterType;
import de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.filter.FilterInstanceCreator;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.attribute.FilterAttributeInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElement.FilterBpmElementInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElementInstance.FilterBpmElementInstanceInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.BaseFilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.KnowledgeBasedFilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result.FilterResult;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.service.IFilterInstancePersistenceService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FilterExecutionServiceImpl implements IFilterExecutionService {

    private IFilterInstancePersistenceService filterInstancePersistenceServiceImpl;
    private FilterInstanceCreator filterInstanceCreator;

    @Override
    public FilterInstance save(FilterInstance instance) {
        return filterInstancePersistenceServiceImpl.save(instance);
    }

    @Override
    public List<FilterInstance> getAll() {
        return filterInstancePersistenceServiceImpl.getAll();
    }

    @Override
    public FilterInstance create(String filterId, Long tasklistId, String assigneeId, FilterType filterType) {
        return filterInstanceCreator.create(new FilterInstance(filterId, tasklistId, assigneeId, filterType));
    }

    @Override
    public FilterInstance createDefault(Long tasklistId, String assigneeId, FilterType filterType) {
        FilterInstance instance = new FilterInstance(assigneeId, tasklistId, filterType);
        return filterInstanceCreator.createDefaultFilter(instance);
    }

    @Override
    public FilterInstance create(FilterInstance instance) {
        return filterInstanceCreator.create(instance);
    }

    @Override
    public FilterInstance create(String filterId, String assigneeId, Tasklist tasklist, FilterType filterType) {
        return filterInstanceCreator.create(new FilterInstance(filterId, assigneeId, tasklist, filterType));
    }

    @Override
    public FilterInstance getLatestByAssigneeId(String assigneeId, Long tasklistId, FilterType filterType) {
        Optional<FilterInstance> dbFilterInstance = filterInstancePersistenceServiceImpl.getLatestByAssigneeIdAndFilterType(assigneeId, filterType);

        if (dbFilterInstance.isEmpty()) {
            return this.createDefault(tasklistId, assigneeId, filterType);
        } else {
            FilterInstance result = this.create(dbFilterInstance.get().getFilterId(), tasklistId, assigneeId, filterType);

            return switch (filterType) {
                case KNOWLEDGE_BASED -> createKnowledgeBased(result, dbFilterInstance.get());
                case BASE -> createBase(result, dbFilterInstance.get());
                default -> result;
            };
        }
    }

    @Override
    public FilterInstance getLatestByFilterInstance(FilterInstance instance) {
        Optional<FilterInstance> dbFilterInstance = filterInstancePersistenceServiceImpl.getLatestByAssigneeIdAndFilterType(instance.getAssigneeId(), instance.getFilterType());

        if (dbFilterInstance.isEmpty()) {
            return this.create(instance);
        } else {
            FilterInstance result = this.create(instance);
            dbFilterInstance.get().setResult(new FilterResult());
            return switch (instance.getFilterType()) {
                case KNOWLEDGE_BASED -> createKnowledgeBased(result, dbFilterInstance.get());
                case BASE -> createBase(result, dbFilterInstance.get());
                default -> result;
            };
        }
    }
    private FilterInstance createKnowledgeBased(FilterInstance base, FilterInstance db) {
        KnowledgeBasedFilterInstance result = (KnowledgeBasedFilterInstance) base;
        Map<String, FilterAttributeInstance> dbAttributes = ((KnowledgeBasedFilterInstance) db).getAttributes().stream().collect(Collectors.toMap(FilterAttributeInstance::getAttributeId, attribute -> attribute));

        result.setAttributes(result.getAttributes().stream().map(attribute -> dbAttributes.getOrDefault(attribute.getAttributeId(), attribute)).toList());

        Map<String, FilterBpmElementInstance> dbBpmElements = ((KnowledgeBasedFilterInstance) db).getElements().stream().collect(Collectors.toMap(FilterBpmElementInstance::getRecproElementId, element -> element));
        result.setElements(result.getElements().stream().map(element -> dbBpmElements.getOrDefault(element.getRecproElementId(), element)).toList());

        Map<String, FilterBpmElementInstanceInstance> dbBpmElementInstances = ((KnowledgeBasedFilterInstance) db).getElementInstances().stream().collect(Collectors.toMap(FilterBpmElementInstanceInstance::getRecproElementInstanceId, element -> element));
        result.setElementInstances(result.getElementInstances().stream().map(elementInstance -> dbBpmElementInstances.getOrDefault(elementInstance.getRecproElementInstanceId(), elementInstance)).toList());

        return result;
    }

    private FilterInstance createBase(FilterInstance base, FilterInstance db) {
        BaseFilterInstance result = (BaseFilterInstance) base;
        BaseFilterInstance dbInstance = (BaseFilterInstance) db;
        result.setAscending(dbInstance.isAscending());
        result.setTasklistOrder(dbInstance.getTasklistOrder());
        return result;
    }



    @Override
    public FilterInstance getByTasklistIdAndFilterType(Long tasklistId, String assigneeId, FilterType filterType) {
        return filterInstancePersistenceServiceImpl.getByTasklistIdAndFilterType(tasklistId, filterType).orElse(createDefault(tasklistId, assigneeId, filterType));
    }
}
