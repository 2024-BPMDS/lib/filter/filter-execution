package de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.filter;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.HybridFilterInstance;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class HybridFilterInstanceCreatorImpl extends FilterInstanceCreatorImpl {
    @Override
    public FilterInstance create(FilterInstance instance, Filter filter) {
        HybridFilterInstance filterInstance = new HybridFilterInstance();

        this.initialize(filterInstance, filter);

        return filterInstance;
    }
}
