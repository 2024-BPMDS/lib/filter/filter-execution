package de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.filter;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.ProcessAwareFilterInstance;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ProcessAwareFilterInstanceCreatorImpl extends FilterInstanceCreatorImpl {
    @Override
    public FilterInstance create(FilterInstance instance, Filter filter) {
        ProcessAwareFilterInstance filterInstance = new ProcessAwareFilterInstance();

        this.initialize(filterInstance, filter);

        return filterInstance;
    }
}
