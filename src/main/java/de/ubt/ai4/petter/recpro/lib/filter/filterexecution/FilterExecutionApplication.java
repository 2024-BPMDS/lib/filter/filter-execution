package de.ubt.ai4.petter.recpro.lib.filter.filterexecution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"de.ubt.ai4.petter.recpro.lib.ontology", "de.ubt.ai4.petter.recpro.lib.bpmpersistence"})
public class FilterExecutionApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilterExecutionApplication.class, args);
	}

}
