package de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.filter;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.ContentBasedFilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ContentBasedFilterInstanceCreatorImpl extends FilterInstanceCreatorImpl {
    @Override
    public FilterInstance create(FilterInstance instance, Filter filter) {
        ContentBasedFilterInstance filterInstance = new ContentBasedFilterInstance();

        this.initialize(filterInstance, filter);

        return filterInstance;
    }
}
