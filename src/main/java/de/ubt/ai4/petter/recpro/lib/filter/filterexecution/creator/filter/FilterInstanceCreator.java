package de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.filter;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.*;
import de.ubt.ai4.petter.recpro.lib.ontology.modeling.filter.base.FilterModelingService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FilterInstanceCreator {
    private final FilterModelingService filterModelingService;
    private final IFilterInstanceCreator baseFilterInstanceCreatorImpl;
    private final IFilterInstanceCreator knowledgeBasedFilterInstanceCreatorImpl;
    private final IFilterInstanceCreator collaborativeFilterInstanceCreatorImpl;
    private final IFilterInstanceCreator contentBasedFilterInstanceCreatorImpl;
    private final IFilterInstanceCreator hybridFilterInstanceCreatorImpl;
    private final IFilterInstanceCreator processAwareFilterInstanceCreatorImpl;

    @Value("${recpro.filter.execution.default.base}")
    private String baseFilter;

    @Value("${recpro.filter.execution.default.knowledge-based}")
    private String knowledgeBasedFilter;

    @Value("${recpro.filter.execution.default.content-based}")
    private String contentBasedFilter;

    @Value("${recpro.filter.execution.default.collaborative}")
    private String collaborativeFilter;

    @Value("${recpro.filter.execution.default.hybrid}")
    private String hybridFilter;

    @Value("${recpro.filter.execution.default.process-aware}")
    private String processAwareFilter;

    @Value("${recpro.filter.execution.default.main}")
    private String mainFilter;

    public FilterInstance createDefaultFilter(FilterInstance filterInstance) {
        if (filterInstance.getFilterType() == null) {
            filterInstance.setFilterId(mainFilter);
            return this.create(filterInstance);
        }

        switch (filterInstance.getFilterType()) {
            case KNOWLEDGE_BASED -> filterInstance.setFilterId(knowledgeBasedFilter);
            case COLLABORATIVE -> filterInstance.setFilterId(collaborativeFilter);
            case CONTENT_BASED -> filterInstance.setFilterId(contentBasedFilter);
            case HYBRID -> filterInstance.setFilterId(hybridFilter);
            case PROCESS_AWARE -> filterInstance.setFilterId(processAwareFilter);
            case BASE -> filterInstance.setFilterId(baseFilter);
            default -> filterInstance.setFilterId(mainFilter);
        }

        return this.create(filterInstance);
    }

    public FilterInstance create(FilterInstance filterInstance) {
        Filter filter = null;

        if (filterInstance.getFilterId() != null) {
            filter = filterModelingService.getById(filterInstance.getFilterId());
        }
        FilterInstance instance;
        if (filter != null && filter.getId() != null) {
            instance = switch (filter.getFilterType()) {
                case KNOWLEDGE_BASED -> knowledgeBasedFilterInstanceCreatorImpl.create(filterInstance, filter);
                case COLLABORATIVE -> collaborativeFilterInstanceCreatorImpl.create(filterInstance, filter);
                case CONTENT_BASED -> contentBasedFilterInstanceCreatorImpl.create(filterInstance, filter);
                case HYBRID -> hybridFilterInstanceCreatorImpl.create(filterInstance, filter);
                case PROCESS_AWARE -> processAwareFilterInstanceCreatorImpl.create(filterInstance, filter);
                case BASE -> baseFilterInstanceCreatorImpl.create(filterInstance, filter);
                case MAIN -> null;
            };
        } else {
            instance = this.createDefaultFilter(filterInstance);
        }
        assert instance != null;
        instance.setTasklist(filterInstance.getTasklist());
        instance.setTasklistId(filterInstance.getTasklistId());
        instance.setAssigneeId(filterInstance.getAssigneeId());
        return instance;
    }
}
